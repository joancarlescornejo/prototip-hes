 #posicionar-se al directori de treball PWD
 mvn clean install

 #Execució docker a partir del jar creat
 docker build -t java-app .
 
 #Execució de l'imatge en un contenidor 
 docker run -dit --name myweb -p 8081:80 java-app
 
 #Prova de la càrrega de l'aplicació
 http://localhost:8081/