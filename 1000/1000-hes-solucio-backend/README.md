#Carpeta on es troba els yml de distribucio de l'aplicació

## Docker generat
Esta prepat per executar la imatge correcte

## Petició feta
Per tal d'executar-ho manualment, seguier els passos del fitxer: 1000/1000-hes-solucio-backend/execute.sh

Al dockerhub: https://hub.docker.com/repositories/joancarlescornejo

Repositoris *joancarlescornejo/hes-solucio-backend*, Tag 
  - [latest](doc:https://hub.docker.com/layers/joancarlescornejo/hes-solucio-backend/latest/images/sha256-ac81db99ea8b3c7717bfbb7995cbb16cd43d31e81057f1a9300118d96fb71e40?context=repo) 
    és la versió d'imatge perque puguis provar en local i amb openshift de local

## Comentaris
El contingut de l'aplicació (*.jar*) creat a la carpeta -> target/dist

## Desplegament Openshift
Per tal de desplegar s'ha de seguir les instruccions del fitxers de la carpeta: 1000/1000-hes-solucio-orchestrators
