package cat.gencat.catsalut.hes.controller;


import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RestController
public class HelloController {

    @RequestMapping(value="/",method = RequestMethod.GET)
    public String index() {
        StringBuilder val = new StringBuilder("Hello HES! ").append(LocalDateTime.now())
        .append("\n").append(" >> Prova de project java base - springboot");
        return val.toString();
    }

}
