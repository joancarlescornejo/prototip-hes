/*

const http = require("http");
const host = 'localhost';
const port = 4001;

const requestListener = function (req, res) {
    res.writeHead(200);
    res.end("Prototip Salut!");
};

const server = http.createServer(requestListener);
server.listen(port, host, () => {
    console.log(`Prototip Salut is running on http://${host}:${port}`);
});

/////////////////////////////////////////////////////////////////////////////
const express = require('express');
const app = express();

app.post('/ruta', (req, res, next) => {
    const data = req.body;
    console.log(data);
    res.status(200).json({
      mensaje: 'Datos recibidos Ok',
      data
    });
});

app.use(express.json()); // for parsing application/json
app.use(express.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
app.listen(4002, host, () => {
    console.log(`Prototip Salut API is running on http://${host}:4002`);
});

*/
var http = require('http'),fs = require('fs');
const host = 'localhost';
const port = 8080;

fs.readFile('./index.html', function (err, html) {
    if (err) {
        throw err; 
    }       
    const server = http.createServer(function(request, response) {  
        response.writeHeader(200, {"Content-Type": "text/html"}); 
        response.write(html);
        response.end();
    });
    server.listen(port, host, () => {
        console.log(`LOG - Prototip Salut is running on http://${host}:${port}`);
    });
});

var copydir = require('copy-dir');
 
copydir.sync('/node_modules', '/../target/dist/node_modules', {
  utimes: true,  // Manté les dades d'inici del arxiu
  mode: true,    // Mantenir els arxius
  cover: true    // arxiu quan existeix, el valor és verdader
});