#Carpeta de codi font pel frontend

## Carpeta /docker
He copiat com a l'exemple: https://git.intranet.gencat.cat/1889/1889-cloud-hes-landing-ui/-/tree/master/src/main/docker
Però per l'exemple no l'he fet servir, però potser a l'hora de copiar si que s'ha d'utilitzar.
Comentat en el "Dockerfile"

## Carpeta /ui
Contingut de la web, en nodejs

## Comentaris
La carpeta del contingut de la web (nodejs) es deixa a la carpeta -> target/dist a l'hora que es construeix
