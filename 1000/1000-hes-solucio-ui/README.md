#Carpeta de codi font pel frontend

## Docker generat
Esta prepat per executar la imatge correcte

## Petició feta
Per tal d'executar-ho manualment, seguier els passos del fitxer: 1000/1000-hes-solucio-ui/execute.sh

Al dockerhub: https://hub.docker.com/repositories/joancarlescornejo

Repositoris *joancarlescornejo/hes-solucio-ui* Tags disponibles
  - [1.0.0](doc:https://hub.docker.com/layers/joancarlescornejo/hes-solucio-ui/1.0.0/images/sha256-041dbb82dd3e054d9f0375d52391bc12a5f1a1350bec6d5a1870e9007a6254cb?context=repo) 
    és la versió d'imatge perque puguis provar en local i amb openshift de local
  - [latest](doc:https://hub.docker.com/layers/joancarlescornejo/hes-solucio-ui/latest/images/sha256-17a07055fe258ac39ce0d8972304fcc6f09ec5d42b10d421610e5fc6b35a1f5f?context=repo)
    és la versió d'imatge amb el dockerfile per opensfhit de ctti

## Comentaris
La carpeta del contingut de la web (nodejs) es deixa a la carpeta -> target/dist /var/www/html

