#posicionar-se al directori de treball PWD
 cd .\src\main\ui | npm install | npm run build
 
 #Tornar a la carpta PWD i executa docker
 docker build -t node-app .

 #En el docker es deixa creada imatge -> cal fer run per provar la web: (copiar target/dist /usr/local/apache2/htdocs i exposar el port 4001)
 docker run --name=nyweb -dit -p 4001:8080 -v ${PWD}/target/dist:/usr/local/apache2/htdocs node-app

 #llavors seria provar amb l'aplicació i veure el resultat
 http://localhost:4001/
